**Description:**

This program is a free software, you can redistribute and/or modify it. 
It was developed for the course of Object-Oriented Programming, faculty of Engineering and Computer Science, University of Bologna, Italy.

**Usage instructions:**

1. Download jar file.
2. Run it.

**Developers:**

* Orazi Federico (IT)
* Raffaelli Simone (IT)
* Borasca Michele (IT)
* Andruccioli Matteo (IT)