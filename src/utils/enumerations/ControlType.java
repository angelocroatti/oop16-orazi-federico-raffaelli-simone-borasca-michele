package utils.enumerations;

/**
 * Defines the types of control of a player. 
 */
public enum ControlType {
    /**
     * The player is controlled by a human.
     */
    HUMAN,

    /**
     * The player is controlled by an AI.
     */
    AI;

}
